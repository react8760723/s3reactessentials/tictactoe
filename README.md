# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

# How to install prettier and enable it for RubyMine
* Execute the following command
  * `npm install --save-dev eslint prettier eslint-config-prettier eslint-plugin-prettier`
  * Enable ESLint validations on save from RubyMine from the settings 
  * Make sure you add the prettier lines to the `.eslintrc.cjs` file, similar to what is in this file.